#pragma strict

private var power = 1200;

var bullet : Transform;		//포탄.

var target : Transform;		//LookAt() 목표.
var searchTarget : Transform;	//찾고자 하는 타겟을 낮은 높이에서부터 찾기 위함.
var spPoint : Transform;	//spawnPoint.
var explosion : Transform;	//포구 앞의 화염.

var snd : AudioClip;	//아군 방향으로 회전.

private var ftime : float = 0.0;				//사격 제한 시간.

function Start () {

}

function Update () {
	transform.LookAt(target);		//아군 방향으로 회전.
	ftime += Time.deltaTime;
	
	
	var hit : RaycastHit;			//탐색 결과 저장.
	//var fwd = Vector3.forward;		//포탑의 전방.
	var fwd = transform.TransformDirection(Vector3.forward);
	
	Debug.DrawRay(spPoint.transform.position, fwd * 200 , Color.green);
	//탐색 실패.
	if(Physics.Raycast(searchTarget.transform.position, fwd, hit, 20) == false)	return;
	if( hit.collider.gameObject.tag != "TANK" || ftime < 0.5 )	return;


	
	//포구앞 화염.
	Instantiate(explosion, spPoint.transform.position , Quaternion.identity);
	
	
	//포탄.
	var obj = Instantiate(bullet, spPoint.transform.position , Quaternion.identity);
	obj.rigidbody.AddForce(fwd*power);
	
	AudioSource.PlayClipAtPoint(snd, spPoint.transform.position);
	ftime = 0;		//경과 시간 리셋.
	
}