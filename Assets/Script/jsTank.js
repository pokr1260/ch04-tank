#pragma strict

var speed = 5;					//이동속도 m/s.
var rotSpeed = 120;				//회전속도 각/s..

var bullet : GameObject;
private var bulletCount : int = 0;
private var cannonCount : int = 10;

var bulletNomal		: GameObject;
var bulletHeavy		: GameObject;
var bulletRocket	: GameObject;

var objLSideGun : GameObject;		//좌측 사이드건.
var objRSIdeGun : GameObject;		//우측 사이드건.

//update
var power = 600;
var Cannon : Transform;
var explosion : Transform;
var snd : AudioClip;
var LSideGun : Transform;
var RSideGun : Transform;

function Start () {

}

function Update () 
{
	var amtToMove = speed * Time.deltaTime;		//프레임에 이동할 거리.
	var amtToRot = rotSpeed * Time.deltaTime;	//프레임에 회전할 각도.
	
	var front 	= Input.GetAxis("Vertical");	//전후진.
	var ang 	= Input.GetAxis("Horizontal");	//좌우 회전 방향.
	var ang2	= Input.GetAxis("MyTank");		//포탑 회전 방향.
	
	transform.Translate(Vector3.forward * front * amtToMove);	//탱크 전후진.
	transform.Rotate(Vector3(0, ang * amtToRot , 0));			//탱크 회전.
	objLSideGun.transform.Rotate(Vector3.up * ang2 * amtToRot);		//포탑 회전.
	objRSIdeGun.transform.Rotate(Vector3.up * ang2 * amtToRot);		//포탑 회전.
	

	if( Input.GetButtonDown("Fire1") )
	{	//총알 발사시.
		var spLPoint = GameObject.Find("LspawnPoint");			//spawnPoint 정보읽기.
		var spRPoint = GameObject.Find("RspawnPoint");
	
	
		//====Left========================================================
		Instantiate(explosion, spLPoint.transform.position , Quaternion.identity);
		AudioSource.PlayClipAtPoint(snd, spLPoint.transform.position);
	
		var myLBullet = Instantiate(bullet , spLPoint.transform.position, spLPoint.transform.rotation);
		myLBullet.rigidbody.AddForce(spLPoint.transform.forward * power );
		
		
		//====Right========================================================
		Instantiate(explosion, spRPoint.transform.position , Quaternion.identity);
		AudioSource.PlayClipAtPoint(snd, spRPoint.transform.position);
	
		var myRBullet = Instantiate(bullet , spRPoint.transform.position, spRPoint.transform.rotation);
		myRBullet.rigidbody.AddForce(spRPoint.transform.forward * power );
		
		if( bulletHeavy == bullet )
		{//헤비머신건의 경우.
			bulletCount -= 8;
			if( 0 >= bulletCount )
			{
				bullet = bulletNomal;
				bulletCount = 0;
			}
		}
		else if( bulletRocket == bullet )
		{//로켓 런쳐의 경우
			bulletCount -= 2;
			if( 0 >= bulletCount )
			{
				bullet = bulletNomal;			
				bulletCount = 0;
			}		
		}
		else
		{
			//딱총의 경우 생략.		
		}

	}
	
	if( Input.GetButtonDown("Fire2") )
	{
		if( 0 < cannonCount )
		{
			
			Debug.Log("press space");
			var spCannonPoint = GameObject.Find("spawnPoint");			//spawnPoint 정보읽기.
		
			Instantiate(explosion, spCannonPoint.transform.position , Quaternion.identity);
			AudioSource.PlayClipAtPoint(snd, spCannonPoint.transform.position);
		
			var myCannon = Instantiate(Cannon , spCannonPoint.transform.position, spCannonPoint.transform.rotation);
			myCannon.rigidbody.AddForce(spCannonPoint.transform.forward * power );
			
			cannonCount--;	
		}
	}

}


function setBullet( getBullet: String )
{
	
	if( "ItemHeavy" == getBullet )
	{
		if( bulletHeavy == bullet )
		{
			bullet = bulletHeavy;
			bulletCount += 150;		
		}
		else
		{
			bullet = bulletHeavy;
			bulletCount = 200;
		}
		
	}
	else if( "ItemRocket" == getBullet )
	{
		if( bulletRocket == bullet )
		{
			bullet = bulletRocket;
			bulletCount += 15;		
		}
		else
		{
			bullet = bulletRocket;
			bulletCount = 30;
		}
	}
}

function OnGUI()
{
	
	if( bulletNomal == bullet )
	{
		GUI.Label(Rect(10,50,120,20), "탄 종류 : 딱총");	
	}
	else if( bulletHeavy == bullet )
	{
		GUI.Label(Rect(10,50,120,20), "탄 종류 : 헤비머신건");		
	}
	else if( bulletRocket == bullet )
	{
		GUI.Label(Rect(10,50,120,20), "탄 종류 : 로켓런쳐");	
	}
	else
	{
		GUI.Label(Rect(10,50,120,20), "탄 종류 : err");	
	}
	GUI.Label(Rect(10,70,120,20), "남은 발수 : " + bulletCount);	
	
	GUI.Label(Rect(10,90,120,20), "캐논 남은 발수 : " + cannonCount);	
		
}

