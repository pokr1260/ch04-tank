#pragma strict

var snd : AudioClip;		//사운드 파일.
var explosion : Transform;

var WinScore : int = 100 ;
var LoseScore : int = 5;

function Start () 
{
	
}

function Update () 
{
	
}

function OnTriggerEnter( coll : Collider )
{	
	Instantiate(explosion, transform.position, Quaternion.identity);
	AudioSource.PlayClipAtPoint(snd, transform.position);
	
	Destroy(gameObject);

	if( "CrashWALL" == coll.gameObject.tag )
	{
		Instantiate(explosion , coll.transform.position, Quaternion.identity);
		//AudioSource.PlayClipAtPoint(snd , transform.position);		//사운드 출력.
		Destroy(coll.gameObject);		//장애물 제거.
		Destroy(gameObject);			//포탄제거.
	}
	else if( coll.gameObject.tag == "ENEMY" )
	{
		if( "BulletHeavy" == gameObject.tag )
		{
			jsScore.hit += 3;
		}
		else if( "BulletRocket" == gameObject.tag )
		{
			jsScore.hit += 5;
		}
		else if( "Cannon" == gameObject.tag )
		{
			jsScore.hit += 10;	
		}
		else 
		{
			jsScore.hit++;
		}
		if( jsScore.hit > WinScore )
		{
			Destroy(coll.transform.root.gameObject);
			//승리화면으로 분기.
			Application.LoadLevel("WinGame");
		}
	
	}
	else if( coll.gameObject.tag == "TANK" && "BulletEnermy" == gameObject.tag )
	{
		jsScore.lose++;
		if( jsScore.lose > LoseScore )
		{
			//패배화면으로 분기.
			Application.LoadLevel("LoseGame");
		}
	
	}
	
}